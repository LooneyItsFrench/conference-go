import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


def process_approval(ch, method, properties, body):
    presentation = json.loads(body)
    return send_mail(
        "Presentation Approved",
        message="Your presentation has been approve: " + presentation["title"],
        from_email="admin@conference.go",
        recipient_list=[presentation["email_address"]],
        fail_silently=False,
    )


channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    presentation = json.loads(body)
    return send_mail(
        "Presentation Rejected",
        message="Your presentation has been rejected: "
        + presentation["title"],
        from_email="admin@conference.go",
        recipient_list=[presentation["email_address"]],
        fail_silently=False,
    )


channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)

channel.start_consuming()
