from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import requests


def get_photo(city, state):
    picture_search = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}", "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    res = requests.get(picture_search, params=params, headers=headers)
    the_json = res.json()
    picture_url = {"picture_url": the_json["photos"][0]["src"]["original"]}
    return picture_url


def get_lat_lon(city, state, country_code):
    current_weather_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},{country_code}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    res = requests.get(current_weather_url, params=params)
    the_json = res.json()
    lon = the_json[0]["lon"]
    lat = the_json[0]["lat"]
    return lat, lon


def get_weather_data(
    city,
    state,
):
    lat, lon = get_lat_lon(city, state, "USA")
    geocoding_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(geocoding_url, params=params)
    the_json = res.json()
    return {
        "description": the_json["weather"][0]["description"],
        "temp": the_json["main"]["temp"],
    }
